#!/bin/bash

ABS_PATH=`cd "$1"; pwd` # double quotes for paths that contain spaces etc...


cd

if [ ! -e .gitconfig ]; then
    ln -s "$ABS_PATH"/.gitconfig
fi

if [ ! -e .git-completion.bash ]; then
    ln -s "$ABS_PATH"/.git-completion.bash
fi

if [ ! -e .git-prompt.sh ]; then
    ln -s "$ABS_PATH"/.git-prompt.sh
fi

if [ ! -e .vimrc ]; then
    ln -s "$ABS_PATH"/.vimrc
fi

if [ ! -e .general ]; then
    ln -s "$ABS_PATH"/.general
fi
echo "source $ABS_PATH/.general" > .profile
if [  ! -f .specific ]; then
    echo "# Put your environment specific aliases here" > .specific
fi
source .profile
update-aliases
