
### How do I get set up? ###

* check out the repo 
* execute the setupShell.sh script
* aaand you're done

### Contribution guidelines ###

This is a very opinion based collection of aliases, which make my work easier. Thus I don't plan on 
accepting any pull requests. If you like what you see but want to adjust it to your personal needs
just do a fork. I will eventually have a look and adjust my aliases if i see something that i like :)
