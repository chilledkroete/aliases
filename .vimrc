set autoread
set number

set so=7
set ruler
set backspace=eol,start,indent
set ignorecase
set hlsearch
set showmatch
set mat=2
set noerrorbells
set novisualbell
set t_vb=
set tm=500

syntax enable
colorscheme elflord
set background=dark

set encoding=utf8
set ffs=unix,dos

set noswapfile

set expandtab
set smarttab
set shiftwidth=4
set tabstop=4


